package Buoi4_ControlFlows;

public class Bai14 {
    //Write a program in Java to display the pattern like right angle triangle with a number like:
    //1
    //12
    //123
    //1234
    public static void main(String[] args)
    {
        int i,j,n=4;

        for(i=1;i<=n;i++)
        {
            for(j=1;j<=i;j++)
                System.out.print(j);

            System.out.println("");
        }
    }
}


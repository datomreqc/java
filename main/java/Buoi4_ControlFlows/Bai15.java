package Buoi4_ControlFlows;

public class Bai15 {
    // Write a program in Java to make such a pattern like right angle triangle with
    //number increased by 1.The pattern like:
    //1
    //2 3
    //4 5 6
    public static void main(String[] args)
    {
        int i,j,n=3,k=1;

        for(i=1;i<=n;i++)
        {
            for(j=1;j<=i;j++)
                System.out.print(k++);
            System.out.println("");
        }
    }

}

package Buoi4_ControlFlows;

import java.util.Scanner;

public class Bai13 {
    //Write a Java program that keeps a number between 1 and 7 from the user and displays the name of the weekday.

    public static void main(String[] args) {
        double celsius;
        int choiceNumber;
        Scanner scanner = new Scanner(System.in);

        // vòng lặp for vắng mặt cả 3 biểu thức
        for (;;) {

            do {
                System.out.println("Bấm số để chọn từ 1 đến 7: ");
                choiceNumber = scanner.nextInt();
            } while ((choiceNumber < 1) || (choiceNumber > 7));

            switch (choiceNumber) {
                case 1: System.out.println("Số bạn chọn tương ứng với: Thứ Hai");break;
                case 2: System.out.println("Số bạn chọn tương ứng với: Thứ Ba");break;
                case 3: System.out.println("Số bạn chọn tương ứng với: Thứ Tư");break;
                case 4: System.out.println("Số bạn chọn tương ứng với: Thứ Năm");break;
                case 5: System.out.println("Số bạn chọn tương ứng với: Thứ Sáu");break;
                case 6: System.out.println("Số bạn chọn tương ứng với: Thứ Bẩy");break;
                case 7: System.out.println("Số bạn chọn tương ứng với: Chủ Nhật");break;
            }
        }
    }
}

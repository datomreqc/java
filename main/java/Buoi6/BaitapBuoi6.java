package Buoi6;
import java.util.Scanner;
public class BaitapBuoi6 {
    // Bài 1: Tính S(n) = 1 + 2 + 3 + … + n.
    public static void main(String[] args) {
        long sum = 0;
        int n;
        Scanner sc;
        do {
            System.out.println("Nhập số tự nhiên n = ");
            sc = new Scanner(System.in);
            n = sc.nextInt();
            if (n<1){
                System.out.println("\nn phải >=1. xin vui lòng nhập lại !");
            }
        }while(n < 1);
        for(int i = 1; i <= n; i++) {
            sum += i;
        }
        System.out.println("Tổng các số tự nhiên từ 1 tới " + n + " là s(n)= "+ sum);
        sc.close();
    }

    //Bai33. Tính biểu thức căn bậc hai

    public static void canBacHai(String[] args) {
        double s ;
        int n;
        Scanner sc;
        do {
            System.out.println("Nhập số tự nhiên n =");
            sc = new Scanner(System.in);
            n = sc.nextInt();
            if (n<1) {
                System.out.println("\nn phải >=1. xin vui lòng nhập lại !");
            }
        }while(n <= 0);
        s = (float) Math.sqrt(2);
        for(int i = 2; i <= n; i++) {
            s = Math.sqrt(2+s);
        }
        System.out.println("Tổng dãy số có n dấu căn s(n)=: "+ s);
        sc.close();
    }
    //Bài 96
    //Viết chương trình nhập giá trị x sau khi tính giá trị của hàm số:
    //𝑓(𝑥) = {2𝑥*x + 5𝑥 + 9 𝑘ℎ𝑖 𝑥 ≥ 5
    //𝑓(𝑥) =-2x*x + 4𝑥 - 9 𝑘ℎ𝑖 𝑥 <5
    public static void bieuThuc(String[] args) {
        float ketqua ;
        float x;
        Scanner sc;

        System.out.println("Nhập giá trị x = ");
        sc = new Scanner(System.in);
        x = sc.nextInt();
        if (x>=5) {
            ketqua = 2 * x * x + 5*x + 9;
            System.out.println("Kết quả của hàm số với x = "+ x +" là " + ketqua);
        }else {
            ketqua = -2 * x * x + 4*x - 9;
            System.out.println("Kết quả của hàm số với x = "+ x +" là " + ketqua);
        }
        sc.close();
    }

    //Bai110: Tim số lượng mệnh giá tiền thỏa mãn biểu thức
    //Cần có tổng 200.000đ từ 3 loại giấy bạc 1000đ, 2000đ, và 5000đ. Lập
    //chương tình để tìm tất cả các phương án có thể.
    public static void loaitien(String[] args) {
        int i,j,k;
        long sum;
        Scanner sc;
        System.out.println("Nhập số tự nhiên sum = ");
        sc = new Scanner(System.in);
        sum = sc.nextInt();
        for ( i = 0; i <= sum/1000; ++i)
            for ( j = 0; j <= sum/2000; ++j)
                for ( k = 0; k <= sum/5000; ++k)
                    if (i * 1000 + j * 2000 + k * 5000 == 200000)
                        System.out.println("so to 1000 là " + i + " ,so to 2000 là " + j + " ,so 5000 là " + k);

    }

}

package Buoi6;
import java.util.Scanner;
public class Bai33 {
    // Bài 1: Tính S(n) = √2 + √2 + √2 + ⋯ √2 + √2 có n dấu căn.
    public static void main(String[] args) {
        double s ;
        int n;
        Scanner sc;
        do {
            System.out.println("Nhập số tự nhiên n =");
            sc = new Scanner(System.in);
            n = sc.nextInt();
            if (n<1) {
                System.out.println("\nn phải >=1. xin vui lòng nhập lại !");
            }
        }while(n <= 0);
        s = (float) Math.sqrt(2);
        for(int i = 2; i <= n; i++) {
            s = Math.sqrt(2+s);
        }
        System.out.println("Tổng dãy số có n dấu căn s(n)=: "+ s);
        sc.close();
    }

}
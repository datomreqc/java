package Buoi8;

import java.awt.*;

public class MovableCricle implements Movable{
    private int radius;
    private MovablePoint center;

    public MovableCricle(int radius, int x, int y, int xSpeed, int ySpeed) {
        this.center = new MovablePoint( x,y,xSpeed,ySpeed);
        this.radius = radius;
    }

    @Override
    public void moveUp() {

    }

    @Override
    public void moveDown() {

    }

    @Override
    public void moveLeft() {

    }

    @Override
    public void moveRight() {

    }
}

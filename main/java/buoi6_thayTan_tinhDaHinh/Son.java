package buoi6_thayTan_tinhDaHinh;

public class Son extends Father{
    public Son(String name, int age){
        super(name, age);
    }
    // Ghi đè phương thức "không có Override vẫn chạy được"
    @Override
    public void Play(){
        super.Play();
        System.out.println("Play basketball");
    }

    public void greeting(){
        System.out.println("Say Hello");
    }
    public int greeting(String greeting){
        System.out.println("Say Đạt");
        return 1;
    }

}

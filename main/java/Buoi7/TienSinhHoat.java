package Buoi7;
import java.io.FileNotFoundException;
import java.util.Formatter;
import java.util.Scanner;

public class TienSinhHoat {
    public static void main(String[] args) {
        int a,b,c,kWh;
        int tiendien,tongtien,tiennuoc,tiennha=2_500_000;
//        int []soDien = {50,100,200,300,400};
//        double[] mucTien = new double[]{1.678, 1.734, 2.014, 2.536, 2.834, 2.927};
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhập vào số điện chốt tháng trước: ");
        a = scanner.nextInt();
        System.out.println("Nhập vào số điện chốt tháng này: ");
        b = scanner.nextInt();
        System.out.println("Nhập số khối nước sử dụng trong tháng: ");
        c = scanner.nextInt();
        kWh = b - a;
        tiennuoc = c*5_000;

        System.out.println("Số điện dùng hết trong tháng là:" +kWh +" "+ "kWh");
        if(kWh >= 0 && kWh <= 50) {
            tiendien = (int) (kWh * 1678);
        }else if(kWh >= 51 && kWh <= 100) {
            tiendien = (int) (50 * 1678 + (kWh-50)*1734);
        }else if(kWh >= 101 && kWh <= 200) {
            tiendien = (int) (50 * 1678 + 50* 1734+(kWh-100)*2014);
        }else if(kWh >= 201 && kWh <= 300) {
            tiendien = (int) (50 * 1678 + 50* 1734+100*2014+(kWh-200)*2536);
        }else if(kWh >= 301 && kWh <= 400) {
            tiendien = (int) (50 * 1678 + 50* 1734+100*2014+100*2536+(kWh-300)*2834);
        }else {
            tiendien = (int)(50 * 1678 + 50* 1734+100*2014+100*2536+100*2834+(kWh-400)*2927);
        }
        tongtien = tiendien + tiennha + tiennuoc;

        Formatter formatter = new Formatter();
        // Use Space format specifier
        formatter.format("%,d", tongtien);
        System.out.println("\nTổng số tiền trong tháng Nam phải trả là:" + formatter + " "+"VNĐ");

    }

}

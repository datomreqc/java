package Buoi11_Set;

import java.util.HashMap;
import java.util.Map;

public class DemoMap {
    public static void main(String[] args) {
        // Map lưu trưc dữ liệu dạng key - value
        // Mỗi key sẽ có 1 value duy nhất
        Map<String, String> pc_and_ip = new HashMap<>();
        // thêm 1 giá trị vào hashmap
        pc_and_ip.put("PC1","192.168.1.010");
        pc_and_ip.put("PC2","192.168.1.011");
         // in ra hashmap
        System.out.println(pc_and_ip);

        // thử thêm 1 key bị trùng sẽ ntn/
        pc_and_ip.put("PC1","192.168.1.012");
        System.out.println(pc_and_ip);

        // lấy 1 giá trị? nếu có trả về value, ko có thì trả về null
        System.out.println(pc_and_ip.get("PC4"));
    }
}

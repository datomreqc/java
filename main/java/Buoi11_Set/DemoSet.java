package Buoi11_Set;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class DemoSet {
    public static void main(String[] args) {
        // Tạo một đối tượng hashset

        HashSet<String> hashSet1 = new HashSet<>();
        hashSet1.add("Hello");
        hashSet1.add("hi");
        hashSet1.add("HeLLo");
        System.out.println(hashSet1);

        // xóa bỏ một phần tử trong set (cần truyền phần tử vào object
        hashSet1.remove("Hello");
        System.out.println(hashSet1);

        // set có thể chuyển đổi
        // truy cập một phần tử -> không truy cập được theo index -> nên ép nó về list để sử dụng
        List<String> arrlist = new ArrayList<>(hashSet1);
        for (int i = 0; i < arrlist.size(); i++) {
            System.out.println(arrlist.get(i));

        }

        // tạo hashset2
        HashSet<String> hashSet2 = new HashSet<>();
        hashSet2.add("hello");
        hashSet2.add("goodbye");
        hashSet1.add("morning");
        // Hợp union -> 2 tập hợp
        HashSet<String> copyHashSet1 = (HashSet<String>) hashSet1.clone();

        copyHashSet1.addAll(hashSet2);
        System.out.println("Phép hợp: "+ copyHashSet1);

        // Phép giao
        copyHashSet1.retainAll(hashSet2);
        System.out.println("Phép giao" + copyHashSet1);

        copyHashSet1 = (HashSet<String>) hashSet1.clone();
        copyHashSet1.retainAll(hashSet2);
        System.out.println("Phép trừ" + copyHashSet1);

    }
}

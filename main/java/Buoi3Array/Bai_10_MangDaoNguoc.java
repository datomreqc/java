package Buoi3Array;
import java.util.Scanner;
import java.util.Arrays;

public class Bai_10_MangDaoNguoc {
    public static void main(String[] args){
        int [] numbers = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        int n = numbers.length;
        System.out.println("Before:" + Arrays.toString(numbers));
//        // V1: Đổi chỗ phần tử đến phần tử ở giữa ( Dùng tính đối xứng)
//
//        for (int i=0; i<n/2; i++){
//            // swap
//            int temp = numbers[i];
//            numbers[i] = numbers[n-i-1];
//            numbers[n-i-1] = temp;
//        }
//        System.out.println("After:" + Arrays.toString(numbers));

        // V2: dùng for
//        for (int left = 0, right = n-1; left < right; left++,right--);{
//            int temp = numbers[left] = numbers [right];
//            numbers[right]=temp;
//        }

        // V3: While
        int i = 0;
        int j= i - 1;
        while(i<j)
        {
            int temp = numbers[i];
            numbers[i] = numbers[j];
            numbers[i] = temp;
            i++;
            j--;
        }
        System.out.print("Mảng sau khi đảo ngược: ");
        for(i=0; i<n; i++)
        {
            System.out.print(numbers[i]+ "  ");
        }
    }
}
 // V4 steam api

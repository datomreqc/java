package BaiTapTongHopOOP.VanBan;

public class Main {
    public static void main(String[] args) {
        VanBan vb = new VanBan("   Cài đặt chương     trình có các   chức năng sau: Cộng, trừ, nhân và    chia 2 số phức.    ");
        System.out.println("Văn bản ban đầu là: ");
        System.out.println(vb.str);
        System.out.println("Văn Bản sau khi chuẩn hóa là: ");
        System.out.println(vb.chuanHoaVB(vb));
        System.out.println("Văn Bản chuyển qua số thường là: ");
        System.out.println(vb.vietThuong(vb));
        System.out.println("Văn bản sau khi viết hoa chữ cái đầu của mỗi từ là: ");
        System.out.println(vb.inHoaChuCaiDau(vb));
        System.out.println("Văn Bản sau khi viết hoa toàn bộ là: ");
        System.out.println(vb.vietHoa(vb));
        System.out.println("Số từ của văn bản là: ");
        System.out.println(vb.demSoTu(vb));
    }
}

package BaiTapTongHopOOP.VanBan;

import java.util.Locale;

public class VanBan implements IVanBan {
    protected String str;

    public VanBan() {
    }

    public VanBan(String st) {
        this.str = st;
    }

    @Override
    public int demSoTu(VanBan vb) {
        int i = 0, count = 1;
        char[] s1 = vb.str.toCharArray();
        while(s1[i] == ' ')
        {
            i++;
        }

        for (int j = i; j < s1.length - 1; j++)
        {
            if(s1[j] == ' ' && s1[j + 1] != ' ')
            {
                count++;
            }
        }
        return count;
    }

    @Override
    public String chuanHoaVB(VanBan vb) {
        str=str.trim();
        str = str.replaceAll("\\s+", " ");
        String[] temp= str.split(" ");
        str="";
        for(int i=0;i<temp.length;i++) {
            str+=String.valueOf(temp[i].charAt(0)) + temp[i].substring(1);
            if(i<temp.length-1) //
                str+=" ";   //
        }
        return str;
    }

    @Override
    public String vietHoa(VanBan vb) {
        vb.str=chuanHoaVB(vb);
        String output = vb.str.toUpperCase();
        return output;
    }

    @Override
    public String vietThuong(VanBan vb) {
        vb.str=chuanHoaVB(vb);
        String output = vb.str.toLowerCase();
        return output;
    }


    @Override
    public String inHoaChuCaiDau(VanBan vb) {
        vb.str= vietThuong(vb);
        String[] arr = vb.str.split("\\s+");
        //dùng vòng lặp duyệt các từ và thay thế từ đầu tiên!
        String output = "";
        for (String x : arr) {
            output = output + (x.substring(0, 1).toUpperCase() + x.substring(1));
            output = output + " ";
        }
        return output;
    }
}

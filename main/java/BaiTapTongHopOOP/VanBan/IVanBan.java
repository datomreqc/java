package BaiTapTongHopOOP.VanBan;


public interface IVanBan {
    public int demSoTu(VanBan vb);
    public String chuanHoaVB(VanBan vb);
    public String vietHoa(VanBan vb);
    public String vietThuong(VanBan vb);
    public String inHoaChuCaiDau(VanBan vb);
}

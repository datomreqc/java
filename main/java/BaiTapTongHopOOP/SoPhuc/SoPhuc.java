package BaiTapTongHopOOP.SoPhuc;

public class SoPhuc {
    private double PhanThuc;
    private double PhanAo;

    public SoPhuc() {
        PhanThuc = 0;
        PhanAo = 0;
    }

    public SoPhuc(double a, double b) {
        this.PhanThuc = a;
        this.PhanAo = b;
    }

    public double getPhanThuc() {
        return PhanThuc;
    }

    public void setPhanThuc(double phanThuc) {
        this.PhanThuc = phanThuc;
    }

    public double getPhanAo() {
        return PhanAo;
    }

    public void setPhanAo(double phanAo) {
        this.PhanAo = phanAo;
    }


    public boolean hienThi() {
        if (PhanAo < 0) {
            System.out.println(PhanThuc + " - " + Math.abs(PhanAo) + "*i");
        } else {
            System.out.println(PhanThuc + " + " + PhanAo + "*i");
        }
        return false;
    }
}

package BaiTapTongHopOOP.SoPhuc;

import java.util.List;

public class TinhToanSoPhuc extends SoPhuc implements ISoPhuc{
    public TinhToanSoPhuc() {
    }

    public TinhToanSoPhuc(double PhanThuc, double PhanAo) {
        super(PhanThuc,PhanAo);
    }

    @Override
    public SoPhuc cong(SoPhuc... args) {
        double thuc = args[0].getPhanThuc();
        double ao = args[0].getPhanAo();
        for (int i = 1; i < args.length ; i++) {
            thuc = thuc + args[i].getPhanThuc();
            ao = ao + args[i].getPhanAo();
        }
        return new SoPhuc(thuc, ao);
    }


    @Override
    public SoPhuc tru(SoPhuc... args) {
        double thuc = args[0].getPhanThuc();
        double ao = args[0].getPhanAo();
        for (int i = 1; i < args.length ; i++) {
            thuc = thuc - args[i].getPhanThuc();
            ao = ao - args[i].getPhanAo();
        }
        return new SoPhuc(thuc, ao);
    }
    @Override
    public SoPhuc nhan(SoPhuc... args) {
        double thuc = args[0].getPhanThuc();
        double ao = args[0].getPhanAo();
        for (int i = 1; i < args.length ; i++) {
            thuc = thuc * args[i].getPhanThuc() - ao * args[i].getPhanAo();
            ao = thuc*args[i].getPhanThuc() + ao * args[i].getPhanAo();
        }
        return new SoPhuc(thuc, ao);
    }

    @Override
    public SoPhuc chia(SoPhuc... args) {
        double thuc = args[0].getPhanThuc();
        double ao = args[0].getPhanAo();
        for (int i = 1; i < args.length ; i++) {
            thuc = (thuc * args[i].getPhanThuc() + ao * args[i].getPhanAo()) / (args[i].getPhanThuc() * args[i].getPhanThuc() + args[i].getPhanAo()*args[i].getPhanAo() );
            ao =  (ao * args[i].getPhanAo() - thuc*args[i].getPhanAo())/(args[i].getPhanThuc() *args[i].getPhanThuc() + args[i].getPhanAo()*args[i].getPhanAo() );
        }
        return new SoPhuc(thuc, ao);
    }
}

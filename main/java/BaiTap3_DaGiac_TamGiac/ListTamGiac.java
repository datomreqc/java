package BaiTap3_DaGiac_TamGiac;
import java.util.Scanner;
public class ListTamGiac extends TamGiac {
    private int n;
    private TamGiac[] list = new TamGiac[100];
    Scanner scanner = new Scanner(System.in);
    public void ListTG(){
        for (int i = 0; i < 100; i++) {
            list[i] = new TamGiac();
        }
    }
    public void List(){
        System.out.println("Nhập số tam giác: ");
        n = scanner.nextInt();
        for (int i = 0; i < n; i++) {
            System.out.println("Nhập thông số các cạnh tam giác: "+ (i + 1) + " :");
            TamGiac tg = new TamGiac();
            tg.nhap();
            list[i] = tg;
        }
    }
    public void TamGiacMax() {
        double max = list[0].tinhDT();
        int Max = 0;
        for (int i = 0; i < n; i++) {
            if (list[i].tinhDT() > max) {
                max = list[i].tinhDT();
                Max = i;
            }
        }
        System.out.println("Tam giác có diện tích lớn nhất là: " + (Max + 1) + " :");
        list[Max].xuat();
    }
    public int tinhCV(){
        int cv=0;
        for (int i = 0; i < n; i++) {
            cv= list[i].tinhCV();
            System.out.println("Chu vi tam giac " + i  + " = " + cv);
        }
        return cv;
    }

    public double tinhDT(){
        double dt=0;
        for (int i = 0; i < n; i++) {
            dt= list[i].tinhDT();
            System.out.println("Diện Tích tam giac " + i  + " = " + dt);
        }
        return dt;
    }

}

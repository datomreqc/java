package baiTapInterface.Bai1;

public interface GeometricObject {
    public double getPerimeter();
    public double getArea();
}

package baiTapInterface.Bai2;

public class HinhTru extends  HinhTron implements  HinhHoc3D{
    private  double chieuCao;

    public HinhTru(double banKinh, double chieuCao) {
        super(banKinh);
        this.chieuCao = chieuCao;
    }

    public double getChieuCao() {
        return chieuCao;
    }

    public void setChieuCao(double chieuCao) {
        this.chieuCao = chieuCao;
    }

    @Override
    public double tinhTheTich() {
        return getBanKinh()*getBanKinh()* chieuCao*Math.PI;
    }

    @Override
    public String toString() {
        return "HinhTru{" + super.toString()+
                "chieuCao=" + chieuCao +
                '}';
    }
}

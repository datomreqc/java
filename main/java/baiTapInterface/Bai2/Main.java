package baiTapInterface.Bai2;

public class Main {
    public static void main(String[] args) {
        HinhBinhHanh hbh = new HinhBinhHanh(5,4,3);
        if (hbh.getCanhBen()<hbh.getChieuCao()) {
            System.out.println("Thông số hình bình hành không hợp lệ");
            System.exit(0);
        }

        System.out.println(hbh.toString());
        System.out.println("Diện Tích Hình Bình Hành là: ");
        System.out.println(hbh.tinhDienTich());
        System.out.println("Chu VI Hình Bình Hành là: ");
        System.out.println(hbh.tinhChuVi());

        //
        HinhChuNhat hcn = new HinhChuNhat(5,4);
        System.out.println(hcn.toString());
        System.out.println("Diện tích hình chữ nhật = ");
        System.out.println(hcn.tinhDienTich());
        System.out.println("Chu Vi Hình chữ nhật = ");
        System.out.println(hcn.tinhChuVi());

        //
        HinhTron ht = new HinhTron(4);
        System.out.println(ht.toString());
        System.out.println("Diện tích hình tròn là: ");
        System.out.println(ht.tinhDienTich());
        System.out.println("Chu Vi hình tròn là: ");
        System.out.println(ht.tinhChuVi());

        //
        HinhTru hinhTru= new HinhTru(5,7);
        System.out.println(hinhTru.toString());
        System.out.println("Thể tích hình trụ là: ");
        System.out.println(hinhTru.tinhTheTich());

    }
}

package baiTapInterface.Bai2;

public class HinhChuNhat extends HinhBinhHanh{


    public HinhChuNhat(double canhDay, double chieuCao) {
        super(canhDay, chieuCao);
    }


    public String toString() {
        return "HinhChuNhat{" + super.toString()+
                '}';
    }
}

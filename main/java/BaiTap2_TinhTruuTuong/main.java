package BaiTap2_TinhTruuTuong;

public class main {
    public static void main(String[] args) {
        Cat cat1 = new Cat("Tom");
        cat1.greets();
        Dog dog1 = new Dog("Mun");
        dog1.greets();
        dog1.greets(dog1);

        BigDog bigDog1 = new BigDog("Toby");
        bigDog1.greets();
        bigDog1.greets(dog1);
        BigDog dog4 = new BigDog("Rosie");
        bigDog1.greets(bigDog1);

    }
}

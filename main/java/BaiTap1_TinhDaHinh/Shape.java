package BaiTap1_TinhDaHinh;

public class Shape {
    private String color;
    private boolean filled;

    public Shape() {
        filled = true;
        color = "red";
    }

    public Shape(String color, boolean filled) {
        this.color = color;
        this.filled = filled;

    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isFilled()
    {
        if (filled == true)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    // A public method for computing the area of Shape
    public void setFilled(boolean filledSetIn) {
        filled = filledSetIn;
    }

    /**
     * Create a new square at default position with default color.
     */
    public String toString()
    {
        String isNot = "";
        if(isFilled() == false)
        {
            isNot = "not ";
        }
        return "Màu " + color + " and is " + isNot + " filled. ";
    }
}

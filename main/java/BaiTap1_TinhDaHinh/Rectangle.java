package BaiTap1_TinhDaHinh;

public class Rectangle extends Shape  //Hình chữ nhật
{
    private double width;
    private double length;

    public Rectangle()
    {
        super();
        width = 1.0;
        length = 1.0;
    }

    public Rectangle(double width, double length)
    {
        super();
        this.width = width;
        this.length = length;
    }

    public Rectangle(double width, double length, String color, boolean filled)
    {
        super (color, filled);
        this.width = width;
        this.length = length;

    }

    public double getWidth()
    {
        return width;
    }

    public void setWidth(double width)
    {
        this.width = width;
    }

    public double getLength()
    {
        return length;
    }

    public void setLength(double length)
    {
        this.length = length;
    }

    public double getArea()  // Diện tích hình chữ nhật
    {
        return length*width;
    }

    public double getPerimeter() // Chu vi hình chữ nhật
    {
        return (2*length)+(2*width);
    }

    public String toString()
    {
        return "Hình chữ nhật với chiều rộng = " + width +" "+ "và chiều dài = " + length + " " + super.toString();
    }
}

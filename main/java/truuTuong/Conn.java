package truuTuong;

public class Conn extends  Cha implements  HanhViMe, HanhViBac{

    public Conn(){

    }

    public Conn(String name, int tuoi) {
        super(name, tuoi);
    }

    @Override
    public void nauAn() {
        System.out.println("Nấu ăn ngon như mẹ");

    }

    @Override
    public void choiNhacCu() {
        System.out.println("Biết chơi piano");

    }

    @Override
    public void caHat() {
        System.out.println("Biết Ca Hát");

    }
}

package truuTuong;

public class Cha {
    private String name;
    private  int tuoi;

    public Cha() {
    }

    public Cha(String name, int tuoi) {
        this.name = name;
        this.tuoi = tuoi;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTuoi() {
        return tuoi;
    }

    public void setTuoi(int tuoi) {
        this.tuoi = tuoi;
    }
    public void choiTheThao(){
        System.out.println("Chơi Bóng rổ");
    }
}

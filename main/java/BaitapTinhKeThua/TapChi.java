package BaitapTinhKeThua;

import java.util.Scanner;

public class TapChi extends Document{
    private int soPhatHanh;

    public TapChi(Scanner sc) {
        this.soPhatHanh = soPhatHanh;
    }

    public TapChi(String maTaiLieu, String tenNhaXuatBan, int soBanPhatHanh, int soPhatHanh) {
        super(maTaiLieu, tenNhaXuatBan, soBanPhatHanh);
        this.soPhatHanh = soPhatHanh;
    }

    public int getSoPhatHanh() {
        return soPhatHanh;
    }

    public void setSoPhatHanh(int soPhatHanh) {
        this.soPhatHanh = soPhatHanh;
    }
    public String toString() {      // in Cylinder class
        String thongtin= "Báo: " + super.toString()  // use Circle's toString()
                + " Số phát hành: " + soPhatHanh;
        return thongtin;
    }
    public void nhapThongTin(Scanner sc) {
        super.nhapThongTin(sc);
//        Scanner scanner = new Scanner(System.in);
        System.out.println("Số Phát Hành là: ");
        this.setSoPhatHanh(sc.nextInt());sc.nextLine();
    }
}

package BaitapTinhKeThua;

import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;



public class ThuVien {

    private ArrayList<Document> dstl=new ArrayList<Document>();

    public void themTaiLieu(Document tl) {
        dstl.add(tl);
    }

    public void nhapTaiLieu(Scanner sc) {
        String traLoi ;
        int choiceNumber;
        System.out.println("Vui lòng chọn  thông tin muốn nhập!");
        System.out.println("1. Nhập vào thông tin sách");
        System.out.println("2. Nhập vào thông tin báo");
        System.out.println("3. Nhập vào thông tin tạp chí ");
        do {
            Document tl = null;
            System.out.println();
            System.out.println("Bấm số để chọn (1/2/3): ");
            choiceNumber = sc.nextInt(); sc.nextLine();

            switch (choiceNumber) {
                case 1:{
                   tl =new Sach(sc);
                    break;
                }
                case 2:{
                    tl=new Bao(sc);
                    break;
                }
                case 3: {
                   tl=new TapChi(sc);
                    break;
                }
                default:{
                    System.out.println("Bạn chỉ có thể chọn 1/2/3");
                    System.exit(1);
                }
            }
            tl.nhapThongTin(sc);//Tinh da hinh
            themTaiLieu(tl);

            System.out.println("Ban muon nhap nua khong? (Y/N): ");
            traLoi = sc.nextLine();
            System.out.println("tra loi length = " + traLoi.length());
        } while ("Y".equalsIgnoreCase(traLoi));
    }

    public void inTaiLieu() {
        for (Document tl : dstl) {
            System.out.println(tl);
        }
    }

    public void timKiemTaiLieuTheoLoai(Scanner sc) {
        System.out.print("Nhap loai tai lieu can tim (Sach,Bao,TapChi): ");
        String loai = sc.nextLine();

        if (loai.equalsIgnoreCase("S")) {
            for (Document tl : dstl) {
                if (tl instanceof Sach) {
                    tl.thongTin();
                }
            }
        } else if (loai.equalsIgnoreCase("B")) {
            for (Document tl : dstl) {
                if (tl instanceof Bao) {
                    tl.thongTin();
                }
            }
        } else if (loai.equalsIgnoreCase("T")) {
            for (Document tl : dstl) {
                if (tl instanceof TapChi) {
                    tl.thongTin();
                }
            }
        }
    }

    public void timKiemTaiLieuTheoMa(String maTaiLieu) {
        for (Document tl : dstl) {
            if (maTaiLieu.equals(tl.getMaTaiLieu())) {
                tl.thongTin();
            }
        }
    }
}

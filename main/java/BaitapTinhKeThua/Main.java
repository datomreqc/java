package BaitapTinhKeThua;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Cylinder c1 = new Cylinder();
        System.out.println(c1);
        System.out.println("Cylinder:"
                + " radius=" + c1.getRadius()
                + " height=" + c1.getHeight()
                + " base area=" + c1.getArea()
                + " volume=" + c1.getVolume());

        Cylinder c2 = new Cylinder(10.0);
        System.out.println(c2);
        System.out.println("Cylinder:"
                + " radius=" + c2.getRadius()
                + " height=" + c2.getHeight()
                + " base area=" + c2.getArea()
                + " volume=" + c2.getVolume());

        Cylinder c3 = new Cylinder(2.0, 10.0);
        System.out.println(c3);
        System.out.println("Cylinder:"
                + " radius=" + c3.getRadius()
                + " height=" + c3.getHeight()
                + " base area=" + c3.getArea()
                + " volume=" + c3.getVolume());

        // Bai2:
        Student hs1= new Student("Đạt","Thành Phố Từ Sơn","Học Java",2022,100);
        System.out.println(hs1);
        Staff nv1= new Staff("Nguyễn Tiến Đạt","Từ Sơn Bắc Ninh","ĐH Bách Khoa HN",100);
        System.out.println(nv1);

        //Bai3:
//        Bao b1 = new Bao("B001","Nguyễn Tiến Đạt",999,22-12-1992);
//        System.out.println(b1.thongTin());
        Scanner sc = new Scanner(System.in);
        Bao b2 = new Bao();
        b2.nhapThongTin(sc);
        System.out.println(b2);
        ThuVien ThuVien = new ThuVien();
        ThuVien.nhapTaiLieu(sc);
        ThuVien.inTaiLieu();





    }
}

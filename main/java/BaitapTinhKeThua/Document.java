package BaitapTinhKeThua;

import java.util.Scanner;

public class Document {
    private String maTaiLieu;
    private String tenNhaXuatBan;
    private int soBanPhatHanh;
    Scanner scanner = new Scanner(System.in);

    public Document() {
        soBanPhatHanh = 50;
    }

    public Document(String maTaiLieu, String tenNhaXuatBan, int soBanPhatHanh) {
        this.maTaiLieu = maTaiLieu;
        this.tenNhaXuatBan = tenNhaXuatBan;
        this.soBanPhatHanh = soBanPhatHanh;
    }


    public String getMaTaiLieu() {
        return maTaiLieu;
    }

    public String getTenNhaXuatBan() {
        return tenNhaXuatBan;
    }

    public int getSoBanPhatHanh() {
        return soBanPhatHanh;
    }

    public void setMaTaiLieu(String maTaiLieu) {
        this.maTaiLieu = maTaiLieu;
    }

    public void setTenNhaXuatBan(String tenNhaXuatBan) {
        this.tenNhaXuatBan = tenNhaXuatBan;
    }

    public void setSoBanPhatHanh(int soBanPhatHanh) {
        this.soBanPhatHanh = soBanPhatHanh;
    }
    public String thongTin() {
        String tt = "Ma Tai Lieu: " + maTaiLieu + "\nNha Xuat Ban: " + tenNhaXuatBan + "\nSo Ban Phat Hanh: " + soBanPhatHanh;
        return tt;
    }

    public String toString() {
        String thongtin = "Mã Tài liệu:" + " " + maTaiLieu +" "+ "Tên nhà xuất bản:"+" "+tenNhaXuatBan+"Số bản phát hành"+" "+soBanPhatHanh;
        return thongTin();
    }

    public void nhapThongTin(Scanner sc) {
        System.out.println("Mã Tài liệu: ");
        maTaiLieu = scanner.nextLine();
//        this.setMaTaiLieu(scanner.nextLine());
        System.out.println("Nhà Xuất Bản là: ");
        tenNhaXuatBan =scanner.nextLine();
//        this.setTenNhaXuatBan(scanner.nextLine());
        System.out.println("Số Bản Phát Hành là: ");
//        this.setSoBanPhatHanh(scanner.nextInt());;
        soBanPhatHanh= scanner.nextInt();
        scanner.nextLine();

    }
}

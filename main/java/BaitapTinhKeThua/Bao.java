package BaitapTinhKeThua;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class Bao extends Document{
    private Date ngayPhatHanh;

    public Bao(Scanner sc) {
    }


    public Bao(String maTaiLieu, String tenNhaXuatBan, int soBanPhatHanh, Date ngayPhatHanh) {
        super(maTaiLieu, tenNhaXuatBan, soBanPhatHanh);
        this.ngayPhatHanh = ngayPhatHanh;
    }

    public Bao() {

    }

    public Date getNgayPhatHanh() {
        return ngayPhatHanh;
    }

    public void setNgayPhatHanh(Date ngayPhatHanh) {
        this.ngayPhatHanh = ngayPhatHanh;
    }


    public String toString() {      // in Cylinder class
        String thongtin= "Student: " + super.toString()  // use Circle's toString()
                + "Date: " + ngayPhatHanh;
        return thongtin;
    }
    public void nhapThongTin(Scanner sc) {
        super.nhapThongTin(sc);
        System.out.print("Ngay phat hanh(dd-mm-yyyy: )");
        String ngayPhatHanh = sc.nextLine();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

        try
        {
            this.ngayPhatHanh = sdf.parse(ngayPhatHanh);
        }catch (Exception e)
        {
            System.out.println();
        }
    }

}


package collection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class ListDemo {
    public static void main(String[] args) {
        // Làm việc với ArrayList

        // VD: Tạo mảng lưu trữ String -> tạo mảng thông thường
        String[] arrString = new  String[30];
        arrString[0] = "hello";
        arrString[1] = "Java";
        // nhược điểm kích thước cố định -> lưu thiếu, không đủ chỗ để lưu or lưu ít dẫn đến dư thừa ( sẽ khó thêm, xóa phần tử)

        // Làm việc với mảng động: không cần khai báo kích thước ban đầu. Khi khai báo thì mảng sẽ tự động tăng ( làm thế nào để thêm vào )

        List<String> arrayList = new ArrayList<>();

        // thêm phần tử
         arrayList.add("Hello");
         arrayList.add("Java");
         arrayList.add("Collection");

         // in ra list
        System.out.println(arrayList);
        // Cập nhật giá trị 1 phần tử

        arrayList.set(0, "Hi");
        System.out.println(arrayList);

        // Xóa phần tử (remove)

        arrayList.remove(1);
        System.out.println(arrayList);

        // Xóa object : cần biết biến tham chiếu đến object đó
        String element = "Test";
        arrayList.add(element);
        System.out.println(arrayList);

        // Xóa theo đối tượng

        arrayList.remove(element);
        System.out.println(arrayList);

        // Sắp xếp

        // Một số phương thức sắp xếp nhanh
        Collections.sort(arrayList);
        System.out.println(arrayList);

        // Mắc định dạng chuỗi sẽ sắp xếp theo alpla
        // Dạng số sẽ sắp xếp theo dạng bé tới lớn

        // Sắp xếp phần tử từ bé đến lớn của 1 arraylist lưu trữ các số thực

        List<Double> arrayList1 = new ArrayList<>();
        arrayList1.add(Double.valueOf("3.0"));
        arrayList1.add(Double.valueOf("5.0"));
        arrayList1.add(Double.valueOf("30.0"));
        arrayList1.add(Double.valueOf("11.0"));
        arrayList1.add(Double.valueOf("13.0"));
        arrayList1.add(Double.valueOf("7.0"));
        arrayList1.add(Double.valueOf("6.0"));
        arrayList1.add(Double.valueOf("99.0"));
        arrayList1.add(Double.valueOf("311.0"));
        arrayList1.add(Double.valueOf("355.0"));
        System.out.println(arrayList1);

        List<Double> arrayList2 = new ArrayList<>();

        Collections.sort(arrayList1);

        // đảo ngược danh sách

        Collections.reverse(arrayList1);
        System.out.println(arrayList1);

        ArrayList<Double> arrayList4 = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            arrayList4.add(new Random().nextDouble());

        }
        System.out.println(arrayList4);
        Collections.sort(arrayList4);














    }
}

package BaiTapBuoi8;

import java.util.Scanner;
import java.util.Date;
import java.text.SimpleDateFormat;
public class ThanhVien
{
    private String hoTen;
    private Date ngaySinh;
    private String ngNghiep;
    private int canCuoc;

    public ThanhVien()
    {
    }
    public ThanhVien(String hoTen, Date ngaySinh, String ngNghiep, int canCuoc) {
        this.hoTen = hoTen;
        this.ngaySinh = ngaySinh;
        this.ngNghiep = ngNghiep;
        this.canCuoc = canCuoc;
    }

    public void nhapThongTin(Scanner sc)
    {
        System.out.print("Nhap ho ten: ");
        this.hoTen=sc.nextLine();
        System.out.print("Nhap ngay sinh theo dinh dang dd-MM-yyyy: ");
        String ns=sc.nextLine();
        this.ngaySinh=chuyenStringDate(ns);
        System.out.print("Nhap nghe nghiep: ");
        this.ngNghiep=sc.nextLine();
        System.out.print("Nhap số căn cước công dân: ");
        this.ngNghiep=sc.nextLine();
    }
    public Date chuyenStringDate(String ngay)
    {
        SimpleDateFormat sdf=new SimpleDateFormat("dd-MM-yyyy");
        Date ns=null;
        try
        {
            ns=sdf.parse(ngay);
        }catch(Exception e)
        {
            System.out.println("Loi dinh dang thoi gian.!");
        }
        return ns;
    }

//    @Override
//    public String toString() {
//        return "ThanhVien{" +
//                "hoTen='" + hoTen + '\'' +
//                ", ngaySinh=" + ngaySinh +
//                ", ngNghiep='" + ngNghiep + '\'' +
//                ", canCuoc=" + canCuoc +
//                '}';
//    }

    public void hienThongTin()
    {
        System.out.println("Họ và tên: "+this.hoTen);
        System.out.println("Ngày sinh: "+this.ngaySinh);
        System.out.println("Nghề nghiệp: "+this.ngNghiep);
        System.out.println("Căn cước: "+this.canCuoc);
    }

    public Date getNgaySinh()
    {
        return this.ngaySinh;
    }

}
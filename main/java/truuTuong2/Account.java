package truuTuong2;

public class Account {
    private String name;
    private String password;
    private String infor;

    public Account(String name, String password, String infor) {
        this.name = name;
        this.password = password;
        this.infor = infor;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getInfor() {
        return infor;
    }

    public void setInfor(String infor) {
        this.infor = infor;
    }
}

package BaiTapGeneric.Bai2;

public class Main {
    public static void main(String[] args) {
        TinhToanSoThuc st = new TinhToanSoThuc();
        System.out.println(st.cong(3.0, 4.0, 5.0));
        System.out.println(st.tru(10.0,4.0));
        System.out.println(st.nhan(3.0,4.0));
        System.out.println(st.chia(6.0,2.0));

        TinhToanPhanSo ps1 = new TinhToanPhanSo(3, 5);
        TinhToanPhanSo ps2 = new TinhToanPhanSo(4, 3);
        TinhToanPhanSo ps3 = new TinhToanPhanSo(1, 2);

        PhanSo t = (ps1.cong(ps2));
        System.out.println(t);
    }
}

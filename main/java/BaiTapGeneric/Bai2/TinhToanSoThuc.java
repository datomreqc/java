package BaiTapGeneric.Bai2;

public class TinhToanSoThuc implements ITinhToan<Double>{


    @Override
    public Double cong(Double... args) {
        double tong = args[0];
        for (int i = 1; i < args.length; i++) {
            tong = tong +  args[i];
        }
        return tong;
    }

    @Override
    public Double tru(Double... args) {
        double hieu =args[0];
        for (int i = 1; i < args.length; i++) {
            hieu = hieu - args[i];
        }
        return hieu;
    }

    @Override
    public Double nhan(Double... args) {
        double tich =args[0];
        for (int i = 1; i < args.length; i++) {
            tich = tich * args[i];
        }
        return tich;
    }

    @Override
    public Double chia(Double... args) {
        double thuong =args[0];
        for (int i = 1; i < args.length; i++) {
            thuong = thuong/args[i];
        }
        return thuong;
    }
}

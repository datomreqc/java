package BaiTapGeneric.Bai2;

public class TinhToanPhanSo extends PhanSo implements ITinhToan<PhanSo>{
    public TinhToanPhanSo() {
    }

    public TinhToanPhanSo(int tuSo, int mauSo) {
        super(tuSo, mauSo);
    }

    @Override
    public PhanSo cong(PhanSo... args) {
        int ts = args[0].getTuSo();
        int ms = args[0].getMauSo();
        for (int i = 1; i < args.length ; i++) {
            ts = ts * args[i].getMauSo() + ms * args[i].getTuSo();
            ms = ms * args[i].getMauSo();
        }
        PhanSo phanSoTong= new PhanSo(ts, ms);
        phanSoTong.toiGianPhanSo();
        return phanSoTong;
    }

    @Override
    public PhanSo tru(PhanSo... args) {
        int ts = args[0].getTuSo();
        int ms = args[0].getMauSo();
        for (int i = 1; i < args.length ; i++) {
            ts = ts * args[i].getMauSo() - ms * args[i].getTuSo();
            ms = ms * args[i].getMauSo();
        }
        PhanSo phanSoHieu= new PhanSo(ts,ms);
        System.out.println(phanSoHieu);
        phanSoHieu.toiGianPhanSo();
        return phanSoHieu;
    }

    @Override
    public PhanSo nhan(PhanSo... args) {
        int ts = args[0].getTuSo();
        int ms = args[0].getMauSo();
        for (int i = 1; i < args.length ; i++) {
            ts = ts * args[i].getTuSo();
            ms = ms * args[i].getMauSo();
        }
        PhanSo phanSoNhan= new PhanSo(ts, ms);
        phanSoNhan.toiGianPhanSo();
        return phanSoNhan;
    }

    @Override
    public PhanSo chia(PhanSo... args) {
        int ts = args[0].getTuSo();
        int ms = args[0].getMauSo();
        for (int i = 1; i <  args.length ; i++) {
            ts = ts * args[i].getMauSo();
            ms = ms * args[i].getTuSo();
        }
        PhanSo phanSoChia= new PhanSo(ts, ms);
        phanSoChia.toiGianPhanSo();
        return phanSoChia;
    }
}

package BaiTapGeneric.Bai1;

public class HinhTron implements IHinhHoc{
    private double banKinh;

    public HinhTron() {
    }

    public HinhTron(double banKinh) {
        this.banKinh = banKinh;
    }

    public double getBanKinh() {
        return banKinh;
    }

    public void setBanKinh(double banKinh) {
        this.banKinh = banKinh;
    }



    @Override
    public double tinhChuVi() {
        double cv = 2*getBanKinh()*Math.PI;
        return cv;
    }

    @Override
    public double tinhDienTich() {
        double dt = getBanKinh()*getBanKinh()*Math.PI;
        return dt;
    }

    @Override
    public String toString() {
        return "HinhTron{" +
                "banKinh=" + banKinh +
                '}';
    }
}

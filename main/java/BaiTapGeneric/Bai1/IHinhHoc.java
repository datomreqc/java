package BaiTapGeneric.Bai1;

public interface IHinhHoc {
    public double tinhChuVi();
    public double tinhDienTich();
}

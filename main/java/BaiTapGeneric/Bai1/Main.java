package BaiTapGeneric.Bai1;

public class Main {
    public static void main(String[] args) {
        HinhVuong hv = new HinhVuong(3);
        System.out.println("Dien Tich Hình Vuông với cạnh = " + hv.getCanh());
        System.out.println(hv.tinhDienTich());
        hv.toString();
        System.out.println("Chu Vi Hình Vuông với cạnh = " + hv.getCanh());
        System.out.println(hv.tinhChuVi());

        HinhTron ht = new HinhTron(4);
        System.out.println("Chu vi hình tròn với bán kính = "+ ht.getBanKinh());
        System.out.println(ht.tinhChuVi());
        System.out.println("Diện Tích hình tròn với bán kính = "+ ht.getBanKinh());
        System.out.println(ht.tinhDienTich());

        Drawable hinhTron = new Drawable<HinhTron>();
        hinhTron.setT(new HinhTron(3));
        hinhTron.draw();

        Drawable hinhVuong = new Drawable<HinhVuong>();
        hinhVuong.setT(new HinhVuong(3));
        hinhVuong.draw();
    }
}

package org.example;

import java.util.Scanner;

public class BaiTapBuoiHaiTinhtong {
    public static void main(String[] args)
    {
        int n, soDu, tong = 0;
        int count = 0;
        System.out.print("Nhap vao mot so nguyen co 3 chu so: n=");
        Scanner sc = new Scanner(System.in);

        n = sc.nextInt();
        if (n<100|| n > 999) {
            System.out.println("Bạn nhập số không hợp lệ");
        }else {
            while (n > 0) {
                soDu = n % 10;
                n = n / 10;
                tong += soDu;
                count +=1;
            }
            System.out.println("Tổng các chữ số của n = " + tong);
            System.out.println("số chữ số của n = " + count);
        }
    }
}


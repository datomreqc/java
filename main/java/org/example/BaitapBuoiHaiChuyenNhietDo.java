package org.example;
import java.util.Scanner;

public class BaitapBuoiHaiChuyenNhietDo {
    public static void main(String[] args) {
        double celsius;
        int choiceNumber;
        Scanner scanner = new Scanner(System.in);

        // vòng lặp for vắng mặt cả 3 biểu thức
        for (;;) {
            System.out.println("1. Chuyển đổ độ F qua dộ C");
            System.out.println("2. Chuyển đổ độ C qua dộ F");

            do {
                System.out.println("Bấm số để chọn (1/2): ");
                choiceNumber = scanner.nextInt();
            } while ((choiceNumber < 1) || (choiceNumber > 2));

            switch (choiceNumber) {
                case 1:
                    System.out.println("Bạn chọn chức năng đổi từ độ Celcius qua độ fahrenheit!");
                    System.out.println("Nhập vào độ Celcius cần đổi: Celcius=");
                    celsius = scanner.nextDouble();
                    double fahrenheit = (double) 9 / 5 * celsius + 32;
                    System.out.println(celsius + " độ C = " + fahrenheit + " độ F");
                    System.exit(0); // thoát chương trình
                    break;
                case 2:
                    System.out.println("Bạn chọn chức năng đổi từ độ fahrenheit qua độ Celcius!");
                    System.out.println("Nhập vào độ fahrenheit cần đổi: fahrenheit = ");
                    fahrenheit = scanner.nextDouble();
                    double Celcius = (double) 5 / 9 * (fahrenheit - 32);
                    System.out.println(fahrenheit + " độ C = " + Celcius + " độ F");
                    System.exit(0); // thoát chương trình
                    break;
            }
        }
    }

}

package BaiTapBuoi9;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static boolean hasObject(File f) {
        // thu doc xem co object nao chua
        FileInputStream fi;
        boolean check = true;
        try {
            fi = new FileInputStream(f);
            ObjectInputStream inStream = new ObjectInputStream(fi);
            if (inStream.readObject() == null) {
                check = false;
            }
            inStream.close();

        } catch (FileNotFoundException e) {
            check = false;
        } catch (IOException e) {
            check = false;
        } catch (ClassNotFoundException e) {
            check = false;
            e.printStackTrace();
        }
        return check;
    }

    public static void write(String s) {
        try {

            File f = new File("result.txt");
            FileOutputStream fo;
            ObjectOutputStream oStream = null;

            // neu file chu ton tai thi tao file va ghi binh thuong
            if (!f.exists()) {
                fo = new FileOutputStream(f);
                oStream = new ObjectOutputStream(fo);
            } else { // neu file ton tai

                // neu chua co thi ghi binh thuong
                if (!hasObject(f)) {
                    fo = new FileOutputStream(f);
                    oStream = new ObjectOutputStream(fo);
                } else { // neu co roi thi ghi them vao

                    fo = new FileOutputStream(f, true);

                    oStream = new ObjectOutputStream(fo) {
                        protected void writeStreamHeader() throws IOException {
                            reset();
                        }
                    };
                }
            }

            oStream.writeObject(s);
            oStream.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void read() {
        try {
            File f = new File("result.txt");
            FileInputStream fis = new FileInputStream(f);
            ObjectInputStream inStream = new ObjectInputStream(fis);
            Object s;
            int i = 0;
            while (true) {
                s = inStream.readObject();
                System.out.println(++i + ":" + s.toString());
            }
        } catch (ClassNotFoundException e) {
        } catch (IOException e) {
        }
    }
    public static PhanSo[] nhapPhanSo(int soLuongPhanSo) {
        Scanner scanner = new Scanner(System.in);
        PhanSo lstPS[] = new PhanSo[soLuongPhanSo];
        for (int i = 0; i < soLuongPhanSo; i++) {
            System.out.println("Mời bạn nhập phân số thứ: " + (i + 1));
            PhanSo ps = new PhanSo();
            ps.nhap();
            lstPS[i] = ps;
        }
        return lstPS;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        TinhToan tt = new TinhToan();
        List<PhanSo> lstKQ = new ArrayList<>();
        int choiceNumber;
        do {
            System.out.println("---------------MENU-------------");
            System.out.println("1. Tính Tổng 3 phân số.");
            System.out.println("2. Tính Hiệu 3 phân số.");
            System.out.println("3. Tính Tích 2 phân số");
            System.out.println("4. Tính Thương 2 phân số");
            System.out.println("5. Lịch sử 5 phép tính gần nhất");

            choiceNumber = Integer.parseInt(scanner.nextLine());
            try {

                switch (choiceNumber) {
                    case 1:
                        PhanSo congPS = tt.cong(nhapPhanSo(3));
                        lstKQ.add(congPS);
                        System.out.println("Tổng 3 phân số là: " + congPS.getTuSo() + "/" + congPS.getMauSo());
                        write(congPS.getTuSo()+ "/"+congPS.getMauSo());
                        write("\n");


                        break;
                    case 2:
                        PhanSo truPS = tt.tru(nhapPhanSo(3));
                        lstKQ.add(truPS);
                        System.out.println("Hiệu 3 phân số là: " + truPS.getTuSo() + "/" + truPS.getMauSo());
                        write(truPS.getTuSo()+ "/"+truPS.getMauSo());
                        write("\n");

                        break;
                    case 3:
                        PhanSo nhanPS = tt.nhan(nhapPhanSo(2));
                        lstKQ.add(nhanPS);
                        System.out.println("Tích 2 phân số là: " + nhanPS.getTuSo() + "/" + nhanPS.getMauSo());
                        write(nhanPS.getTuSo()+ "/"+nhanPS.getMauSo());

                        break;
                    case 4:
                        PhanSo chiaPS = tt.chia(nhapPhanSo(2));
                        lstKQ.add(chiaPS);
                        System.out.println("Thuong 2 phan so la: " + chiaPS.getTuSo() + "/" + chiaPS.getMauSo());
                        write(chiaPS.getTuSo()+ "/"+chiaPS.getMauSo());

                        break;
                    case 5:
                        System.out.println("Ket qua cac phep tinh la: ");
                        for (int i = 0; i < 5; i++) {
                            System.out.println(lstKQ.get(i).getTuSo() + "/" + lstKQ.get(i).getMauSo());
                        }
                        System.out.println("Lich su 5 phep tinh gan nhat");
                        //	System.out.println(lstKQ .toString());
                        read();
                        break;
                    default:
                        System.out.println("Khong hop le");
                        break;
                }
            } catch (NumberFormatException e) {
            }
        } while (choiceNumber != 5);
        scanner.close();

    }
}


package Buoi5_ThayTan_TinhKeThua.TinhKethua;

public class Cha {
    private String hoTen;
    private int namSinh;

    // Phương thức tạo ra đối tượng
    public Cha(){
    }

    public Cha(String hoTen, int namSinh) {
        this.hoTen = hoTen;
        this.namSinh = namSinh;
    }

    public String getHoTen() {
        return hoTen;
    }

    public int getNamSinh() {
        return namSinh;
    }

    public void setHoTen(String hoTen) {
        this.hoTen = hoTen;
    }

    public void setNamSinh(int namSinh) {
        this.namSinh = namSinh;
    }
    public void an(){
        System.out.println("Cha-Ăn uống khỏe mạnh");
    }
    public void ngu(){
        System.out.println("Cha-Cần ngủ nghỉ đầy đủ");
    }
    private void test(){}
    protected void test1(){
        System.out.println("Test");
    }

}

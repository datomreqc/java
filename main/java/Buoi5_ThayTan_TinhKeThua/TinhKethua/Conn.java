package Buoi5_ThayTan_TinhKeThua.TinhKethua;

public class Conn extends Cha {
    private String mauDa;

    //Contructor
    public Conn(){
    }
    public Conn(String hoten, int namSinh, String mauDa){
        // Gọi đến contructor Cha
        super(hoten, namSinh);
        this.mauDa = mauDa;
    }

    public String getMauDa() {
        return mauDa;
    }

    public void setMauDa(String mauDa) {
        this.mauDa = mauDa;
    }
    public void hoc(){
        System.out.println("Con-Học hành chăm chỉ");
    }
    public void choi(){
        System.out.println("Con-Thích chơi thể thao");
    }

}

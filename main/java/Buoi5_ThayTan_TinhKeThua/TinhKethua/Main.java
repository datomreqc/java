package Buoi5_ThayTan_TinhKeThua.TinhKethua;

public class Main {
    public static void main(String[] args) {
        //Tạo 2 đối tượng
        Cha cha = new Cha("cha",1980);

        Conn con = new Conn("Con Trai", 2005, "màu vàng");
        con.an();
        con.ngu();
        con.test1();

        // Con kế thừa các thuộc tính, phương thức của lớp cha
        // Ép kiểu dữ liệu?
        Cha cha1 = new Conn("Con Gái", 2008,"Trắng");
        Conn connn1 = (Conn) cha1;
        System.out.println(connn1.getMauDa());

        // Quản lý thư viện: Base tailieu, Sách extends tailieu, bao extends Tailieu
        // Ko biết lưu sách bao nhiêu, báo là nbao nhiêu
        // ArrayList<TaiLieu> có thể lưu sách hoặc báo

        // ArrayList<Sach>, ArrayList<Bao>


    }
}

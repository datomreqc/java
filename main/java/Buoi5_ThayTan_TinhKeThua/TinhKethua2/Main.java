package Buoi5_ThayTan_TinhKeThua.TinhKethua2;

public class Main {
    public static void main(String[] args) {
        HinhChuNhat hcn = new HinhChuNhat(3, 4);
        // Ngầm định gọi đến toString() luôn
        System.out.println(hcn.toString());
        HinhVuong hv = new HinhVuong(4);
        System.out.println(hv.toString());
    }
}
package BaiTapBuoi12;

import java.util.ArrayList;

@FunctionalInterface
interface Processor {
    int getStringLength(String str);
}

public class Main {


    public static void main(String[] args) {
        String name = "Java Lambda dat";

        // 1. Đếm số từ trong chuỗi
        Processor soTuTrongChuoi = (String str) -> {
            int i = 0, count = 1;
            char[] s1 = name.toCharArray();
            while (s1[i] == ' ') {
                i++;
            }

            for (int j = i; j < s1.length - 1; j++) {
                if (s1[j] == ' ' && s1[j + 1] != ' ') {
                    count++;
                }
            }
            return count;
        };
        System.out.println("Số từ trong chuỗi là: ");
        System.out.println(soTuTrongChuoi.getStringLength(name));

        // 2. Đếm số kí tự trong chuỗi
        String name2 = name.replace(" ","");
        Processor soKiTuTrongChuoi = (String str) -> {
            return name2.length();
        };
        System.out.println("Số ký tự trong chuỗi la");
        System.out.println(soKiTuTrongChuoi.getStringLength(name));

        // Đếm số lần ký tự "a" xuất hiện trong chuỗi
        char kyTu = 'a';
        Processor soTuXuatHienTrongChuoi = (String str) -> {
            int count = 0;
            // duyệt từ đầu đến cuối chuỗi
            for (int i = 0; i < name.length(); i++) {
                // Nếu ký tự tại vị trí thứ i bằng 'a' thì tăng count lên 1
                if (name.charAt(i) == kyTu) {
                    count++;
                }
            }
            return count;
        };
        System.out.println("Số ký tự" + " "+ kyTu+ " xuất hiện trong chuỗi là: ");
        System.out.println(soTuXuatHienTrongChuoi.getStringLength(name));

        // 4. Đếm số kí tự được sử dụng trong chuỗi
        String name1 = name.replace(" ","");
        Processor soKiTuSuDungTrongChuoi = (String str) -> {
            int i,j;
            ArrayList<Character> items=new ArrayList <Character>();
            ArrayList <Integer>count=new ArrayList <Integer>();
            items.add(0,name1.charAt(0));
            count.add(0,0);

            for(j=0;j<name1.length();j++){
                for(i=0;i<items.size();i++){
                    if(name1.charAt(j)==items.get(i)){
                        int temp=0;
                        temp=count.get(i)+1;
                        count.add(i,temp);
                        break;
                    }
                }
                if(i==items.size()){
                    items.add(i,name1.charAt(j));
                    count.add(i,1);
                }
            }
            for (i = 0; i < items.size(); i++)
                System.out.printf("%c\t %d\n", items.get(i), count.get(i));
            System.out.println("\n");
            return items.size();
        };
        System.out.println("Số ký tự được sử dụng của chuỗi là: ");
        System.out.println(soKiTuSuDungTrongChuoi.getStringLength(name));

    }
}

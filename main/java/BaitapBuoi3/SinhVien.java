package BaitapBuoi3;

public class SinhVien {
    //Khai báo thuộc tính
    int MSV;
    String hoTen;
    float DiemLT;
    float DiemTH;

    //khởi tạo phương thức constructor không có tham số
    public SinhVien() {
    }

    // khởi tạo phương thức constructor có tham số
    public SinhVien(int MSV, String hoTen, float DiemLT, float DiemTH) {
        this.MSV = MSV;
        this.hoTen = hoTen;
        this.DiemLT = DiemLT;
        this.DiemTH = DiemTH;
    }

    //getter và setter cho mỗi thuộc tính
    public int getMSV() {
        return MSV;
    }
    public String getHoTen() {
        return hoTen;
    }
    public float getDiemLT() {
        return DiemLT;
    }
    public float getDiemTH() {
        return DiemTH;
    }
    public void setMSV(int MSV) {
        this.MSV = MSV;
    }
    public void setHoTen(String hoTen) {
        this.hoTen = hoTen;
    }
    public void setDiemLT(float DiemLT) {
        this.DiemLT = DiemLT;
    }
    public void setDiemTH(float DiemTH) {
        this.DiemTH = DiemTH;
    }


    //Phương thức tính điểm trung bình
    public double DiemTB() {
        return (DiemLT + DiemTH)/2;
    }

    public String toString(){//ghi de phuong thuc toString()
        return MSV+" "+hoTen+" "+ DiemLT+" "+ DiemTH + " " +DiemTB();
    }
    public void inSV() {
        System.out.printf("%-14d %-18s %10.2f %15.2f %18.2f \n", MSV, hoTen, DiemLT, DiemTH, DiemTB());
    }

}

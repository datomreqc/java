package Buoi4_TinhDongGoi;

public class Student {
    // Thuộc tính
    private int ud;
    private String name;
    boolean gender; // true Name or false name

    public Student(int ud, String name, boolean gender) {
        this.ud = ud;
        this.name = name;
        this.gender = gender;
    }

    // Phương thức khởi tạo constuctor: alt + ins
    public Student(){
    }

    public int getUd() {
        return ud;
    }

    public String getName() {
        return name;
    }

    public boolean isGender() {
        return gender;
    }

    public void setUd(int ud) {
        this.ud = ud;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setGender(boolean gender) {
        this.gender = gender;
    }

}

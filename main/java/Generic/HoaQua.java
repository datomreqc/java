package Generic;

public class HoaQua {
    protected int gia;
    protected String ten;
    protected String mauSac;

    public HoaQua() {
    }

    public HoaQua(int gia, String ten, String mauSac) {
        this.gia = gia;
        this.ten = ten;
        this.mauSac = mauSac;
    }


    @Override
    public String toString() {
        return "HoaQua{" +
                "gia=" + gia +
                ", ten='" + ten + '\'' +
                ", mauSac='" + mauSac + '\'' +
                '}';
    }
}

package BaiTapbuoi4;

public class Calculator{


    public PhanSo congPhanSo(PhanSo obj1, PhanSo obj2) {
        int ts = obj1.getTuSo() * obj2.getMauSo() + obj1.getMauSo() * obj2.getTuSo();
        int ms = obj1.getMauSo() * obj2.getMauSo();
        PhanSo phanSoTong = new PhanSo(ts, ms);
        phanSoTong.toiGianPhanSo();
//        System.out.println("Tổng hai phân số = " + phanSoTong.getTuSo() + "/" + phanSoTong.getMauSo());
        return phanSoTong;
    }
    public PhanSo truPhanSo(PhanSo obj1, PhanSo obj2) {
        int ts = obj1.getTuSo() * obj2.getMauSo()- obj2.getTuSo() *obj1.getMauSo();
        int ms = obj1.getMauSo() * obj2.getMauSo();
        PhanSo phanSoHieu = new PhanSo(ts, ms);
        phanSoHieu.toiGianPhanSo();
//        System.out.println("Hiệu hai phân số = " + phanSoHieu.getTuSo() + "/" + phanSoHieu.getMauSo());
        return phanSoHieu;
    }

    public PhanSo nhanPhanSo(PhanSo obj1, PhanSo obj2) {
        int ts = obj1.getTuSo() * obj2.getTuSo();
        int ms = obj1.getMauSo() * obj2.getMauSo();
        PhanSo phanSoTich = new PhanSo(ts, ms);
        phanSoTich.toiGianPhanSo();
//        System.out.println("Tích hai phân số = " + phanSoTich.getTuSo() + "/" + phanSoTich.getMauSo());
        return phanSoTich;
    }

    public PhanSo chiaPhanSo(PhanSo obj1, PhanSo obj2) {
        PhanSo obj3;
        int ts = obj1.getTuSo() * obj2.getMauSo();
        int ms = obj1.getMauSo() * obj2.getTuSo();
        PhanSo phanSoChia = new PhanSo(ts, ms);
        phanSoChia.toiGianPhanSo();
//        System.out.println("Thương hai phân số = " + phanSoChia.getTuSo() + "/" + phanSoChia.getMauSo());
        return phanSoChia;

    }

}

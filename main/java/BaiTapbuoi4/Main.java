package BaiTapbuoi4;

public class Main {
    public static void main(String[] args) {
        System.out.println("Bai1: Phép tính cộng, trừ, nhân, chia 2 phân số");

        Calculator Calculator = new Calculator();
        PhanSo obj1=new PhanSo(9,3);
        PhanSo obj2=new PhanSo(4,8);
        Calculator.congPhanSo(obj1,obj2);
        System.out.println("Tổng hai phân số = " + Calculator.congPhanSo(obj1,obj2).getTuSo() + "/" + Calculator.congPhanSo(obj1,obj2).getMauSo());
        Calculator.truPhanSo(obj1,obj2);
        System.out.println("Hiệu hai phân số = " + Calculator.truPhanSo(obj1,obj2).getTuSo() + "/" + Calculator.truPhanSo(obj1,obj2).getMauSo());
        Calculator.nhanPhanSo(obj1,obj2);
        System.out.println("Tích hai phân số = " + Calculator.nhanPhanSo(obj1,obj2).getTuSo() + "/" + Calculator.nhanPhanSo(obj1,obj2).getMauSo());
        Calculator.chiaPhanSo(obj1,obj2);
        System.out.println("Thương hai phân số = " + Calculator.chiaPhanSo(obj1,obj2).getTuSo() + "/" + Calculator.chiaPhanSo(obj1,obj2).getMauSo());

        System.out.println("\nBai2: Diện tích hình tròn");
        System.out.println("\nIn ra diện tích hình tròn với bán kính mặc định:");
        Circle c1 = new Circle();
        System.out.println(c1);
        System.out.println("\nIn ra diện tích hình tròn với bán kính mới truyền vào:");
        Circle Hinhtron = new Circle(2);
        System.out.println(Hinhtron);

        System.out.println("\nBai3: Thông tin nhân viên và lương");


        Employee NV = new Employee(8,"Nguyen","Tien Dat",5000);
        System.out.println(NV);
        System.out.println("\nSet giá trị salary mới cho nhân viên");
        NV.setSalary(10000);
        System.out.println(NV);
        System.out.println("Thu nhập 1 năm của nhân viên là: "+ NV.getAnnualSalary());
        System.out.println("Lương mới của nhân viên là: " + NV.raiseSalary(10));
        System.out.println("Thu nhập 1 năm của nhân viên sau tăng lương là: "+ NV.getAnnualSalary());
        System.out.println(NV);




    }

}




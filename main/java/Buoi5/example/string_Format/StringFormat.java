package Buoi5.example.string_Format;

import java.util.Date;
import java.lang.String;

public class StringFormat {
    //số, số thập phân, các date time
    public static void main(String[] args)
    {
        // examples
        System.out.printf("Interger: %d\n",20);

        // Interger số nguyên
        System.out.printf("%d\n",123);  // in ra số nguyên
        System.out.printf("%6d\n",123); // in ra 6 ký tự căn ở bên phải_ việc format dữ liệu rất quan trọng
        System.out.printf("%-6d\n",123); // in ra 6 ký tự căn ở bên trái "-d"
        System.out.printf("%06d\n",123); // in ra 6 ký tuwh căn bên phải ký tự nào thiếu thay bằng số 0

        // Floats Kiểu thập phân lấy default 6 số thập phân (có làm tròn)
        System.out.printf("%f\n", 10.123456789);
        System.out.printf("%15f\n", 10.123456789);
        System.out.printf("%f.8f\n", 10.123456789); //có 8 chữ số thập phân sau số phẩy
        System.out.printf("%f9.4f\n", 10.123456789); // cho 9 vị trí và lấy 4 số sau dấu phẩy
        // String
        System.out.printf("%s\n", "Java String Formatting");
        System.out.printf("%30s\n", "Java String Formatting");
        System.out.printf("%-30s\n", "Java String Formatting");
        System.out.printf("%.15s\n", "Java String Formatting");

        // other examples
        // Integer: 20
        // Float: 10.123456789


        System.out.printf("Integer:"+"%d\n",20);
        System.out.printf("Integer:"+"%.3f\n",10.123456789);
        System.out.printf("String: %s; Interger: %d; Float: %.5f\n","OMG",20,10.123456789);

        // Date, time

        System.out.printf("Current time - %tT\n", new Date());
        System.out.printf("Current time - %tT\n", new Date());

//        String longDate= String.format("Today is %tA %<tB %,td, %<tY, new Date");
//        System.out.println(longDate);

        //index
        System.out.printf("%3$10s %2$10s %1$10s", "group","Mount","one");






    }
}

package Buoi5.example.string_Format;
import java.util.*;

public class Bai20 {
    public static void main(String[] args) {
        // create Formatter class object
        //Space format specifier
        System.out.println("===== Space format specifier ====");

        Formatter formatter = new Formatter();
        // Use Space format specifier
        formatter.format("%d",-111);
        System.out.println(formatter);

        formatter = new Formatter();
        formatter.format("%3d|",12);
        System.out.println(formatter);

        formatter = new Formatter();
        formatter.format("%d",-222);
        System.out.println(formatter);

        formatter = new Formatter();
        formatter.format("%4d",222);
        System.out.println(formatter);


        // + Sign Specifier
        // Thêm dấu "+" trong số dương
        System.out.println("===== + Sign Specifier =====");

        formatter = new Formatter();
        formatter.format("%+d", 111);
        System.out.println(formatter);
        // Dấu "-" sẽ k có hiệu lực
        formatter = new Formatter();
        formatter.format("%+d", -111);
        System.out.println(formatter);

        //  specifier: Đặt các giá trị âm trong dấu ngoặc còn các số dương thì không
        System.out.println("===== ( specifier =====");

        formatter = new Formatter();
        formatter.format("%(d", -111);
        System.out.println(formatter);

        formatter = new Formatter();
        formatter.format("%(d", 111);
        System.out.println(formatter);

        // Comma, Specifier: Thêm dấu phân tách nhóm bằng dấu ","
        System.out.println("===== Comma, Specifier =====");

        formatter = new Formatter();
        formatter.format("%, d", 1000000);
        System.out.println(formatter);
        // thêm dấu phẩy và hiển thị đến số chữ 3 của số thập phân

        formatter = new Formatter();
        formatter.format("%, .3f", 32659526566.4521101);
        System.out.println(formatter);

        // Căn mặc định trái và phải
        System.out.println("===== Left Justification(-) Specifier =====");
        //Căn phải
        formatter = new Formatter();
        formatter.format("|%20.4f|", 1234.1234);
        System.out.println(formatter);

        // Căn Trái
        formatter = new Formatter();
        formatter.format("|%-20.4f|", 1234.1234);
        System.out.println(formatter);

        System.out.println("===== Precision Formats =====");

        formatter = new Formatter();
        formatter.format("%16.2e", 123.1234567);// 1.23e+02= 1.23 x 10^2 16.2 là in ra 16 phần tử căn trái và 2 phần tử thập phân
        System.out.println("Scientific notation to 2 places: " + formatter);

        // Decimal floating-point with Format 4 decimal places.
        formatter = new Formatter();
        formatter.format("%.4f", 123.1234567);
        System.out.println("Decimal floating-point" + " notation to 4 places: " + formatter);

        // %g %G:  Uses %e or %f, whichever is shorter and Format 4 places.
        formatter = new Formatter();
        formatter.format("%.4g", 123.1234567);
        System.out.println("Scientific or Decimal floating-point " + "notation to 4 places: " + formatter);

        // Display at most 15 characters in a string.
        formatter = new Formatter();
        formatter.format("%.15s", "12345678901234567890");
        System.out.println("String notation to 15 places: " + formatter);

        // Format into 10 digit
        formatter = new Formatter();
        formatter.format("%010d", 88);
        System.out.println("value in 10 digits: " + formatter);


    }
}

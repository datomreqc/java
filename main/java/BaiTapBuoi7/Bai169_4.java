package BaiTapBuoi7;

public class Bai169_4 {
    public static void main(String[] args) {
        int[] array = {-5,-3,-6,1,5,67,34,12};
        int min = array[0];
        int dem = 0;
        int dem1 = 0;
        for (int i = 0; i <= array.length - 1; i++) {
            if (array[i] % 2 != 0) {
                dem++;
                if (array[i] < min) min = array[i];
            }
        }
        if (dem == 0) {
            System.out.println("Hàm không có số lẻ nên không thỏa mãn điều kiện bài toán");
            System.exit(0);
        }

        System.out.println("\nSố lẻ có trong mang la: " + dem);

        System.out.println("Số lẻ nhỏ nhất trong mảng là: " + min);
        int j = array[0];
        for (int i = 0; i <= array.length - 1; i++) {
            if (array[i] % 2 == 0) {
                if (array[i] < min) {
                    dem1++;
                    if (array[i] > j)
                        j = array[i];
                }
            }
        }
        if (dem1 == 0) {
            System.out.println("Mảng không có số chẵn nào nhỏ hơn số lẻ nhỏ nhất");
            System.exit(1);
        }
        System.out.println("Số số chẵn nhỏ hơn số lẻ nhỏ nhất là: " + dem1);

        System.out.println("Số chẵn lớn nhất nhỏ hơn mọi số lẻ trong mảng là: " + j);

    }
}


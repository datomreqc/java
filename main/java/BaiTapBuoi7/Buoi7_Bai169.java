package BaiTapBuoi7;

import java.util.Scanner;

public class Buoi7_Bai169 {
    //
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhập vào số phần tử của mảng: ");
        int n = scanner.nextInt();
        do {
            if (n < 1) {
                System.out.println("\nn phải >=1. xin vui lòng nhập lại !");
            }
        }
        while (n < 0);
        int array[] = new int[n];

        System.out.println("Nhập các phần tử cho mảng: ");
        for (int i = 0; i < n; i++) {
            System.out.print("Nhập phần tử thứ " + i + ": ");
            array[i] = scanner.nextInt();
        }
        scanner.close();

        System.out.println("Mảng ban đầu: ");
        for (int i = 0; i < n; i++) {
            System.out.print(array[i] + "\t");
        }

        int min = array[0];
        int dem = 0;
        int dem1 = 0;
        for (int i = 0; i <= n - 1; i++) {
            if (array[i] % 2 != 0) {
                dem++;
                if (array[i] < min) min = array[i];
            }
        }
        if (dem == 0) {
            System.out.println("Hàm không có số lẻ nên không thỏa mãn điều kiện bài toán");
            System.exit(0);
        }

        System.out.println("\nSố lẻ có trong mang la: " + dem);

        System.out.println("Số lẻ nhỏ nhất trong mảng là: " + min);
        int j = array[0];
        for (int i = 0; i <= n - 1; i++) {
            if (array[i] % 2 == 0) {
                if (array[i] < min) {
                    dem1++;
                    if (array[i] > j)
                        j = array[i];
                }
            }
        }
        if (dem1 == 0) {
            System.out.println("Mảng không có số chẵn nào nhỏ hơn số lẻ nhỏ nhất");
            System.exit(1);
        }
        System.out.println("Số số chẵn nhỏ hơn số lẻ nhỏ nhất là: " + dem1);

        System.out.println("Số chẵn lớn nhất nhỏ hơn mọi số lẻ trong mảng là: " + j);

    }
}

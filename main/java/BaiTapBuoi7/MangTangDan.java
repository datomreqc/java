package BaiTapBuoi7;

public class MangTangDan {
    public int MangTangDan(int n) {
        int array[] = new int[n], tempSort = 0;
        for (int i = 0; i < n - 1; i++) {
            for (int j = i + 1; j <= n - 1; j++) {
                if (array[i] > array[j]) {
                    tempSort = array[i];
                    array[i] = array[j];
                    array[j] = tempSort;
                }
            }
        }
        return tempSort;
    }
}


package BaiTapBuoi7;

public class SapXepLaiMang {
    public static void SapXep(int[] array) {
        int tempSort = 0;
        for (int i = 0; i < array.length - 1; i++) {
            for (int j = i + 1; j <= array.length - 1; j++) {
                if (array[i] > array[j]) {
                    tempSort = array[i];
                    array[i] = array[j];
                    array[j] = tempSort;
                }
            }
        }
        System.out.println("\nMảng sắp xếp theo thứ tự Tăng dần là: ");
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + "\t");
        }
    }

    public static void main(String args[]) {
        int a[] = { 33, 3, 4, 5 };
        SapXep(a);// truyền mảng tới phương thức
        System.out.println("\n phần tử nhỏ nhất là: "+ a[0]);
    }
}

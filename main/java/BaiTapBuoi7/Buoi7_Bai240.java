package BaiTapBuoi7;

public class Buoi7_Bai240 {
    public static int kiemtraGiaTri0(int arrays[]) {
        //Bai 240.Hãy kiểm tra mảng số nguyên có tồn tại giá trị không hay không? Nếu
        //không tồn tại giá trị không trả về giá trị 0, ngược lại trả về 1
        //(tontaikhong).
        int x = 0;
        for (int i = 0; i < arrays.length; i++) {
            if (arrays[i] == 0) {
                x = 1;
            }
        }
        return x;
    }
    public static void main(String[] args){
        int[] arrays= {1,3,5,7,9};
        int x = kiemtraGiaTri0(arrays);
        if (x==1) {
            System.out.println("\nMảng có giá trị 0");
        }
        else {
            System.out.println("\nMảng không có giá trị 0");
        }
    }
}

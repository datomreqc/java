package function_lambda;

public class Main2 {
    public static void main(String[] args) {
        MyCalulator myCalulator = new MyCalulator();
        System.out.println(myCalulator.cong(4, 5));
        // muốn tính phép tính (2a+b) *3
        double rs = myCalulator.doSomeThing((a, b) -> {
            return (2 * a + 5 * b) + 3;
        }, 4, 5);
        System.out.println(rs);

        // tính giá trị 2^3

        double rs1 = myCalulator.doSomeThing((a, b) -> {
            return Math.pow(a, b);
        }, 2, 3);
        System.out.println(rs1);

        // Tính biểu thức bất kì: 2a + 5b+10 -> bóc tách gồm có toán hạng, toán tử( phép tính: nhân & cộng) -> cho phép phép nào tính trước phép nào tính sau


        // Viết thông thường, không theo lambda: (a+b)^2

        double rs2 = myCalulator.doSomeThing(new MyCalulator.Operator() {
            @Override
            public double values(double a, double b) {
                return (a + b) * (a + b);
            }
        }, 2, 3);
        System.out.println(rs2);

        double rs3 = myCalulator.doSomeThing((a, b) -> {
            return (a + b) * (a + b);
        }, 2, 3);
        System.out.println(rs3);


    }
}

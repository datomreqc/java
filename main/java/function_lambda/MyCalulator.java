package function_lambda;

public class MyCalulator {
    // Chỉ cho phép cộng trừ nhân chia cơ bản, đồng thời cho phép người dùng định nghĩa nhanh các phép tính

    interface Operator{
        double values(double a , double b);
    }
    public double cong(double a, double b){
        return  a+b;
    }
    public double tru(double a, double b){
        return a - b;
    }

    // Do something
    public double doSomeThing(Operator operator, double a, double b){
        return operator.values(a,b);
    }
}

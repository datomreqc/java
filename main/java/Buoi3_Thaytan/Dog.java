package Buoi3_Thaytan;

public class Dog {
    // Thuộc tính của lớp - biến
    String breed;
    String size;
    int age;
    String color;

    // hành vi - phương thức ( method, function)
    void eat (){
        System.out.println("Chó" + breed +" là động vật ăn tạp");
    }
    void run(){
        System.out.println("Chó" + size +"chạy nhanh");
    }
    void sleep(){
        System.out.println("ZzzzZ");
    }
}

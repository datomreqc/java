package Buoi3_Thaytan;
import java.util.Calendar;
import java.util.Objects;
public class NhanVien {
    // Thuộc tính
    String hoTen;
    String gioiTinh;
    int namSinh;
    // Constructor: phương thức khởi tạo
    public NhanVien(){

    }
    // Có tham số ngay lúc khởi tạo
    public NhanVien(String hoTen, String gioiTinh, int namSinh){
        this.hoTen = hoTen;
        this.namSinh = namSinh;
        this.gioiTinh = gioiTinh;
    }

    // Hành vi: method
    String getHoTen(){
        if(gioiTinh.equals("Nam")){
            return "Mr."+ hoTen;
        }
        else return "Ms."+ hoTen;
    }
    int getAge(){
        return Calendar.getInstance().get(Calendar.YEAR) - namSinh;
    }
}
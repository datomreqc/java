package BaiTapBuoi13;


import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;

public class Main {
    public static double danhSachSinhVien(Function<List<Student>, Double> fn, List<Student> studentList) {
        return fn.apply(studentList);
    }

    public static void printStringWithCondition(Predicate<Student> predicate, List<Student> studentList) {
        for (Student sv : studentList) {
            if (predicate.test(sv)) {
                System.out.println(sv);
            }
        }
    }
    public static void main(String[] args) {
        ArrayList<Student> danhSachSV = new ArrayList<>();
        Student sv1 = new Student("sv01", "Ngô Tiến Đạt", 18, "Nam", 80);
        Student sv2 = new Student("sv02", "Lê Tiến Đạt", 25, "Nam", 40);
        Student sv3 = new Student("sv03", "Nguyễn Tiến Đạt", 19, "Nam", 49);
        Student sv4 = new Student("sv04", "Phạm Tiến Đạt", 19, "Nam", 80);
        Student sv5 = new Student("sv05", "Nguyễn Thị Vui", 22, "Nữ", 80);
        danhSachSV.add(sv1);
        danhSachSV.add(sv2);
        danhSachSV.add(sv3);
        danhSachSV.add(sv4);
        danhSachSV.add(sv5);

        for (int i = 0; i < danhSachSV.size(); i++) {
            System.out.println(danhSachSV.get(i));
        }

        // 1. In ra tuổi trung bình của các sinh viên trong danh sách.
        double tuoiTB = danhSachSinhVien((a) -> {
            double t = 0;
            for (int i = 0; i < a.size(); i++) {
                t += a.get(i).getAge();
            }
            return t / a.size();
        }, danhSachSV);
        System.out.println("Điểm trung bình của các sinh viên là: ");
        System.out.println(tuoiTB);

        //2. In ra điểm số cao nhất.
        double diemMax = danhSachSinhVien((a) -> {
            double t = a.get(0).getMark();
            for (int i = 1; i < a.size(); i++) {
                if (a.get(i).getMark() >= t) {
                    t = a.get(i).getMark();
                }
            }
            return t;
        }, danhSachSV);
        System.out.println("Điểm số cao nhất là: ");
        System.out.println(diemMax);

        //3. In ra điểm số thấp nhất
        double diemMin = danhSachSinhVien((a) -> {
            double t = a.get(0).getMark();
            for (int i = 1; i < a.size(); i++) {
                if (a.get(i).getMark() <= t) {
                    t = a.get(i).getMark();
                }
            }
            return t;
        }, danhSachSV);
        System.out.println("Điểm số thấp nhất là: ");
        System.out.println(diemMin);

        //4. In ra điểm số phổ biến nhất
        double diemPhoBienNhat = danhSachSinhVien((a) -> {
            int i, j;
            ArrayList<Double> items = new ArrayList<Double>();
            ArrayList<Integer> count = new ArrayList<Integer>();
            items.add(0, (double) a.get(0).getMark());
            count.add(0, 0);

            for (j = 0; j < a.size(); j++) {
                for (i = 0; i < items.size(); i++) {
                    if (a.get(j).getMark() == items.get(i)) {
                        int temp = 0;
                        temp = count.get(i) + 1;
                        count.add(i, temp);
                        break;
                    }
                }
                if (i == items.size()) {
                    items.add(i, (double) a.get(j).getMark());
                    count.add(i, 1);
                }
            }
            int max = 1;
            for (i = 0; i < items.size(); i++) {
                if (count.get(i) > max)
                    max = count.get(i);

            }
            double diem = 0;
            for (i = 0; i < items.size(); i++) {
                if (count.get(i) == max) diem = items.get(i);
            }
            return diem;
        }, danhSachSV);
        System.out.println("Điểm số phổ biến nhất là: ");
        System.out.println(diemPhoBienNhat);

        // - Sử dụng Predicate:
        //+ In ra thông tin của những người có tuổi lớn hơn 19.
        System.out.println("Danh sách sinh viên có tuổi lớn hơn 19 : \n");
        printStringWithCondition(new Predicate<Student>() {
            @Override
            public boolean test(Student student) {
                if (student.getAge() > 19) return true;
                return false;
            }

        }, danhSachSV);

        // In ra thông tin các nữ sinh viên.
        System.out.println("Danh sách các sinh viên nữ là: \n");
        printStringWithCondition(new Predicate<Student>() {
            @Override
            public boolean test(Student student) {
                if (student.getGender().contains("Nữ")) return true;
                return false;
            }

        }, danhSachSV);

        // In ra thông tin những bạn điểm dưới < 50.
        System.out.println("Danh sách các sinh viên điểm dưới < 50: \n");
        printStringWithCondition(new Predicate<Student>() {
            @Override
            public boolean test(Student student) {
                if (student.getMark() < 50) return true;
                return false;
            }

        }, danhSachSV);


        // In ra những bạn có họ Nguyễn.
        System.out.println("Danh sách các sinh viên có họ Nguyễn: \n");
        printStringWithCondition(new Predicate<Student>() {
            @Override
            public boolean test(Student student) {
                if (student.getGender().contains("Nguyễn")) return true;
                return false;
            }

        }, danhSachSV);

    }
}
